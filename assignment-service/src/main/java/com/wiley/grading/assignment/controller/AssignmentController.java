package com.wiley.grading.assignment.controller;

import com.wiley.grading.assignment.dto.ResponseDto;
import com.wiley.grading.assignment.service.CourseAssignmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("assignment")
public class AssignmentController {
    private final CourseAssignmentService courseAssignmentService;

    public AssignmentController(CourseAssignmentService courseAssignmentService) {
        this.courseAssignmentService = courseAssignmentService;
    }

    @GetMapping("course/{course-id}")
    public ResponseEntity<ResponseDto> findAssignmentsByCourseId(@PathVariable("course-id") String courseId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(courseAssignmentService.findAssignmentsByCourseId(courseId))
                        .build());

    }
}
