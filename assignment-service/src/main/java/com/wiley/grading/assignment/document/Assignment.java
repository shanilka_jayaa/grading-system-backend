package com.wiley.grading.assignment.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "assignment")
public class Assignment {
    @Id
    private ObjectId assignmentId;
    private String name;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;


    public String getAssignmentId() {
        return this.assignmentId.toString();
    }
}
