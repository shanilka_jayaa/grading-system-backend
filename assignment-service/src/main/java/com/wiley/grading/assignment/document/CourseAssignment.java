package com.wiley.grading.assignment.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "courseAssignment")
public class CourseAssignment {
    @Id
    private ObjectId courseAssignmentId;
    @Indexed
    private String courseId;
    private String assignmentId;
    private String startDate;
    private String endDate;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;

    public String getCourseAssignmentId() {
        return this.courseAssignmentId.toString();
    }
}
