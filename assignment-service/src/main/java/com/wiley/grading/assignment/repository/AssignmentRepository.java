package com.wiley.grading.assignment.repository;

import com.wiley.grading.assignment.document.Assignment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AssignmentRepository extends MongoRepository<Assignment, String> {
}
