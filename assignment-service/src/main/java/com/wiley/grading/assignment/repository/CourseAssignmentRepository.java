package com.wiley.grading.assignment.repository;

import com.wiley.grading.assignment.document.CourseAssignment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CourseAssignmentRepository extends MongoRepository<CourseAssignment, String> {
    List<CourseAssignment> findAllByCourseId(String courseId);
}
