package com.wiley.grading.assignment.service;

import com.wiley.grading.assignment.document.Assignment;

import java.util.List;

public interface AssignmentService {
    Iterable<Assignment> findAssignmentsByIds(List<String> assignmentIds);
}
