package com.wiley.grading.assignment.service;

import com.wiley.grading.assignment.document.Assignment;

public interface CourseAssignmentService {
    Iterable<Assignment> findAssignmentsByCourseId(String courseId);
}
