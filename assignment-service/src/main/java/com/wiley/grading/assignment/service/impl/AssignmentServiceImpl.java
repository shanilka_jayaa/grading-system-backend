package com.wiley.grading.assignment.service.impl;

import com.wiley.grading.assignment.document.Assignment;
import com.wiley.grading.assignment.exception.NotFoundException;
import com.wiley.grading.assignment.repository.AssignmentRepository;
import com.wiley.grading.assignment.service.AssignmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AssignmentServiceImpl implements AssignmentService {
    private final AssignmentRepository assignmentRepository;

    public AssignmentServiceImpl(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    @Override
    public Iterable<Assignment> findAssignmentsByIds(List<String> assignmentIds) {
        Iterable<Assignment> assignments = assignmentRepository.findAllById(assignmentIds);
        if (assignments != null && assignments.iterator().hasNext()) {
            return assignments;
        }
        log.error("No available assignments for these assignment Ids " + assignmentIds);
        throw new NotFoundException("No available assignments for these assignment Ids " + assignmentIds);
    }
}
