package com.wiley.grading.assignment.service.impl;

import com.wiley.grading.assignment.document.Assignment;
import com.wiley.grading.assignment.document.CourseAssignment;
import com.wiley.grading.assignment.exception.NotFoundException;
import com.wiley.grading.assignment.repository.CourseAssignmentRepository;
import com.wiley.grading.assignment.service.AssignmentService;
import com.wiley.grading.assignment.service.CourseAssignmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CourseAssignmentServiceImpl implements CourseAssignmentService {
    private final CourseAssignmentRepository courseAssignmentRepository;
    private final AssignmentService assignmentService;

    public CourseAssignmentServiceImpl(CourseAssignmentRepository courseAssignmentRepository, AssignmentService assignmentService) {
        this.courseAssignmentRepository = courseAssignmentRepository;
        this.assignmentService = assignmentService;
    }

    @Override
    public Iterable<Assignment> findAssignmentsByCourseId(String courseId) {
        List<String> assignmentIds = courseAssignmentRepository
                .findAllByCourseId(courseId)
                .stream()
                .map(CourseAssignment::getAssignmentId)
                .collect(Collectors.toList());
        if (assignmentIds == null || assignmentIds.isEmpty()) {
            log.error("No available assignments for courseId" + courseId);
            throw new NotFoundException("No available assignments for courseId" + courseId);
        }
        return assignmentService.findAssignmentsByIds(assignmentIds);
    }
}
