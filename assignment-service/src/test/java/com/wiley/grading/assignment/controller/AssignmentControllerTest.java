package com.wiley.grading.assignment.controller;

import com.wiley.grading.assignment.document.Assignment;
import com.wiley.grading.assignment.exception.NotFoundException;
import com.wiley.grading.assignment.service.CourseAssignmentService;
import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
class AssignmentControllerTest {

    List<Assignment> assignmentDocuments = new ArrayList();
    private MockMvc mvc;
    @Mock
    private CourseAssignmentService courseAssignmentService;
    @InjectMocks
    private final AssignmentController assignmentController = new AssignmentController(courseAssignmentService);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(assignmentController).build();

        Assignment assignment = new Assignment();
        assignment.setAssignmentId(ObjectId.get());
        assignmentDocuments.add(assignment);
    }

    @Test
    public void contextLoads() {
        Assertions.assertThat(assignmentController).isNotNull();
    }

    @Test
    void findAssignmentsByCourse() throws Exception {
        Mockito.when(courseAssignmentService.findAssignmentsByCourseId("test"))
                .thenReturn(assignmentDocuments);
        mvc.perform(get("/assignment/course/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }

    @Test
    void findAssignmentsByCourseNotFundException() throws Exception {
        Mockito.when(courseAssignmentService.findAssignmentsByCourseId("test"))
                .thenThrow(new NotFoundException("test"));
        mvc.perform(get("/assignment/course/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}