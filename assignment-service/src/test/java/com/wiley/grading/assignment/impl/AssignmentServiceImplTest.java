package com.wiley.grading.assignment.impl;

import com.wiley.grading.assignment.document.Assignment;
import com.wiley.grading.assignment.exception.NotFoundException;
import com.wiley.grading.assignment.repository.AssignmentRepository;
import com.wiley.grading.assignment.service.AssignmentService;
import com.wiley.grading.assignment.service.impl.AssignmentServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AssignmentServiceImplTest {

    List<String> idList = new ArrayList<>();
    @Mock
    private AssignmentRepository assignmentRepository;
    @InjectMocks
    private final AssignmentService assignmentService = new AssignmentServiceImpl(assignmentRepository);

    @BeforeEach
    void setUp() {
        idList.add("abc");
    }

    @Test
    void findAssignmentsByIdsValidResponse() {
        List<Assignment> assignments = new ArrayList();
        Assignment assignment = new Assignment();
        assignment.setAssignmentId(ObjectId.get());
        assignments.add(assignment);
        Mockito.when(assignmentRepository.findAllById(idList))
                .thenReturn(assignments);
        assertEquals(assignments,
                assignmentService.findAssignmentsByIds(idList));
    }

    @Test
    void findAssignmentsByIdsNotFound() {
        List<Assignment> assignments = new ArrayList();
        Mockito.when(assignmentRepository.findAllById(idList))
                .thenReturn(assignments);
        Assertions.assertThrows(NotFoundException.class, () -> {
            assignmentService.findAssignmentsByIds(idList);
        });
    }
}