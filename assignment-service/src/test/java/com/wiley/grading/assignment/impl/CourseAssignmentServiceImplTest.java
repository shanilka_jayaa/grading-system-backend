package com.wiley.grading.assignment.impl;

import com.wiley.grading.assignment.document.Assignment;
import com.wiley.grading.assignment.document.CourseAssignment;
import com.wiley.grading.assignment.exception.NotFoundException;
import com.wiley.grading.assignment.repository.CourseAssignmentRepository;
import com.wiley.grading.assignment.service.AssignmentService;
import com.wiley.grading.assignment.service.CourseAssignmentService;
import com.wiley.grading.assignment.service.impl.CourseAssignmentServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CourseAssignmentServiceImplTest {

    List<String> idList = new ArrayList<>();
    List<Assignment> assignmentList = new ArrayList();
    @Mock
    private CourseAssignmentRepository courseAssignmentRepository;
    @Mock
    private AssignmentService assignmentService;
    @InjectMocks
    private final CourseAssignmentService courseAssignmentService = new CourseAssignmentServiceImpl(
            courseAssignmentRepository,
            assignmentService);

    @BeforeEach
    void setUp() {
        idList.add("abc");
        Assignment assignment = new Assignment();
        assignment.setAssignmentId(ObjectId.get());
        assignmentList.add(assignment);
    }

    @Test
    void findAssignmentsByCourseValidResponse() {
        List<CourseAssignment> courseAssignments = new ArrayList<>();
        CourseAssignment courseAssignment = new CourseAssignment();
        courseAssignment.setCourseAssignmentId(ObjectId.get());
        courseAssignment.setAssignmentId("abc");
        courseAssignments.add(courseAssignment);

        Mockito.when(courseAssignmentRepository.findAllByCourseId("abc"))
                .thenReturn(courseAssignments);
        Mockito.when(assignmentService.findAssignmentsByIds(idList))
                .thenReturn(assignmentList);
        assertEquals(assignmentList,
                courseAssignmentService.findAssignmentsByCourseId("abc"));
    }


    @Test
    void findAssignmentsByCourseNotFound() {
        List<CourseAssignment> courseAssignmentDocuments = new ArrayList<>();
        Mockito.when(courseAssignmentRepository.findAllByCourseId("abc"))
                .thenReturn(courseAssignmentDocuments);
        Assertions.assertThrows(NotFoundException.class, () -> {
            courseAssignmentService.findAssignmentsByCourseId("abc");
        });
    }
}