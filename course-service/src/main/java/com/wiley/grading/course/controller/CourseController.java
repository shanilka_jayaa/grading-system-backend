package com.wiley.grading.course.controller;

import com.wiley.grading.course.dto.ResponseDto;
import com.wiley.grading.course.service.CourseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("course")
public class CourseController {
    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("find-by-course-ids")
    public ResponseEntity<ResponseDto> findCoursesByIds(@RequestParam("id") String[] courseIds) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(courseService.findCoursesByIds(courseIds))
                        .build());
    }
}
