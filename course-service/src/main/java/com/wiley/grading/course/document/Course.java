package com.wiley.grading.course.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "course")
public class Course {
    @Id
    private ObjectId courseId;
    private String name;
    private String startDate;
    private String endDate;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;


    public String getCourseId() {
        return this.courseId.toString();
    }
}
