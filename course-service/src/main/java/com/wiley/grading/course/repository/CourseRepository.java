package com.wiley.grading.course.repository;

import com.wiley.grading.course.document.Course;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CourseRepository extends MongoRepository<Course, String> {
    List<Course> findAllByCourseIdIn(List<String> courseIds);
}
