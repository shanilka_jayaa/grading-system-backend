package com.wiley.grading.course.service;

import com.wiley.grading.course.document.Course;

import java.util.List;

public interface CourseService {
    List<Course> findCoursesByIds(String[] courseIds);
}
