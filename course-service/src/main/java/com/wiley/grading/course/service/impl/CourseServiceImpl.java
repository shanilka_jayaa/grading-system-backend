package com.wiley.grading.course.service.impl;

import com.wiley.grading.course.document.Course;
import com.wiley.grading.course.exception.NotFoundException;
import com.wiley.grading.course.repository.CourseRepository;
import com.wiley.grading.course.service.CourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> findCoursesByIds(String[] courseIds) {
        List<Course> courseDocuments = courseRepository.findAllByCourseIdIn(Arrays.asList(courseIds));
        if (courseDocuments != null && !courseDocuments.isEmpty()) {
            return courseDocuments;
        }
        log.error("No courses found for courseIds " + Arrays.asList(courseIds));
        throw new NotFoundException("No courses found for courseIds " + Arrays.asList(courseIds));
    }
}
