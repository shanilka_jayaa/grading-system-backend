package com.wiley.grading.course.controller;

import com.wiley.grading.course.document.Course;
import com.wiley.grading.course.exception.NotFoundException;
import com.wiley.grading.course.service.CourseService;
import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class CourseControllerTest {

    String[] courseIds = {"abc", "def"};
    private MockMvc mvc;
    @Mock
    private CourseService courseService;
    @InjectMocks
    private final CourseController courseController = new CourseController(courseService);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(courseController).build();
    }

    @Test
    public void contextLoads() {
        Assertions.assertThat(courseController).isNotNull();
    }


    @Test
    void findCoursesByIdsShouldReturnResponse() throws Exception {
        List<Course> courseList = new ArrayList<>();

        Course course1 = new Course();
        course1.setCourseId(ObjectId.get());
        Course course2 = new Course();
        course2.setCourseId(ObjectId.get());
        courseList.add(course1);
        courseList.add(course2);

        Mockito.when(courseService.findCoursesByIds(courseIds))
                .thenReturn(courseList);
        mvc.perform(get("/course/find-by-course-ids?id=abc&id=def")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }

    @Test
    void findCoursesByIdsNotFundException() throws Exception {
        Mockito.when(courseService.findCoursesByIds(courseIds))
                .thenThrow(new NotFoundException("12345"));
        mvc.perform(get("/course/find-by-course-ids?id=abc&id=def")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}