package com.wiley.grading.enrollment.controller;

import com.wiley.grading.enrollment.dto.ResponseDto;
import com.wiley.grading.enrollment.service.EnrollmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("enrollment")
public class EnrollmentController {
    private final EnrollmentService enrollmentService;


    public EnrollmentController(EnrollmentService enrollmentService) {
        this.enrollmentService = enrollmentService;
    }

    @GetMapping("find-enrollments-by-user/{user-id}")
    public ResponseEntity<ResponseDto> findEnrollmentsByUserId(@PathVariable("user-id") String userId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(enrollmentService.findEnrollmentsByUserId(userId).getData())
                        .build());
    }
}
