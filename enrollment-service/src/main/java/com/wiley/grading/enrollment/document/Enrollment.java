package com.wiley.grading.enrollment.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "enrollment")
public class Enrollment {
    @Id
    private ObjectId enrollmentId;
    @Indexed
    private String userId;
    @Indexed
    private String courseId;
    private String enrollmentStartDate;
    private String enrollmentEndDate;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;


    public String getEnrollmentId() {
        return this.enrollmentId.toString();
    }
}
