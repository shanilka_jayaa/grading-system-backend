package com.wiley.grading.enrollment.exception;

import com.netflix.client.ClientException;
import com.wiley.grading.enrollment.dto.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ResponseDto> handlerNotFoundException(
            Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ResponseDto
                        .builder()
                        .status("error")
                        .data(ExceptionDto.builder().message(ex.getMessage()).build())
                        .build());

    }

    @org.springframework.web.bind.annotation.ExceptionHandler({ServiceUnavailable.class, ClientException.class})
    public ResponseEntity<ResponseDto> handleServiceUnavailable(
            Exception ex) {
        log.error(ex.getMessage());

        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body(ResponseDto
                        .builder()
                        .status("error")
                        .data(ExceptionDto.builder().message(ex.getMessage()).build())
                        .build());

    }
}
