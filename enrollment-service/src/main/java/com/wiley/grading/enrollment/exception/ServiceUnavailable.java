package com.wiley.grading.enrollment.exception;

public class ServiceUnavailable extends Throwable {
    public ServiceUnavailable(String message) {
        super(message);
    }
}
