package com.wiley.grading.enrollment.feign;

import com.wiley.grading.enrollment.dto.ResponseDto;
import com.wiley.grading.enrollment.exception.ServiceUnavailable;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RefreshScope
@FeignClient(name = "${course-service.application.name}", fallbackFactory = CourseServiceClientFallbackFactory.class)
public interface CourseServiceClient {

    @GetMapping("/course/find-by-course-ids")
    ResponseEntity<ResponseDto> findCoursesByIds(@RequestParam("id") String[] courseIds) throws ServiceUnavailable;

}


