package com.wiley.grading.enrollment.feign;

import com.wiley.grading.enrollment.exception.ServiceUnavailable;
import feign.FeignException;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CourseServiceClientFallbackFactory implements FallbackFactory<CourseServiceClient> {
    @Override
    public CourseServiceClient create(Throwable cause) {

        String httpStatus = cause instanceof FeignException ? Integer.toString(((FeignException) cause).status()) : "";

        return courseIds -> {
            log.info("Feign call exception {} {}", httpStatus, cause.getMessage());
            throw new ServiceUnavailable("Feign call failed..");
        };
    }
}
