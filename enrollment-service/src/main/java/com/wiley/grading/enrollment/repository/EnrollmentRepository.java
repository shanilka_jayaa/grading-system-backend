package com.wiley.grading.enrollment.repository;

import com.wiley.grading.enrollment.document.Enrollment;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EnrollmentRepository extends MongoRepository<Enrollment, String> {
    List<Enrollment> findAllByUserId(String userId, Sort sort);
}
