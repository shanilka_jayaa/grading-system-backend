package com.wiley.grading.enrollment.service;

import com.wiley.grading.enrollment.dto.ResponseDto;

public interface EnrollmentService {
    ResponseDto findEnrollmentsByUserId(String userId);
}
