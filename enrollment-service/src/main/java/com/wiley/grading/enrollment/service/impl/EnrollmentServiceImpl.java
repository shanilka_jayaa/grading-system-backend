package com.wiley.grading.enrollment.service.impl;

import com.wiley.grading.enrollment.document.Enrollment;
import com.wiley.grading.enrollment.dto.ResponseDto;
import com.wiley.grading.enrollment.exception.NotFoundException;
import com.wiley.grading.enrollment.exception.ServiceUnavailable;
import com.wiley.grading.enrollment.feign.CourseServiceClient;
import com.wiley.grading.enrollment.repository.EnrollmentRepository;
import com.wiley.grading.enrollment.service.EnrollmentService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class EnrollmentServiceImpl implements EnrollmentService {
    private final EnrollmentRepository enrollmentRepository;
    private final CourseServiceClient courseServiceClient;


    public EnrollmentServiceImpl(EnrollmentRepository enrollmentRepository, CourseServiceClient courseServiceClient) {
        this.enrollmentRepository = enrollmentRepository;
        this.courseServiceClient = courseServiceClient;
    }

    @Override
    public ResponseDto findEnrollmentsByUserId(String userId) {
        List<Enrollment> enrollmentDocuments = getEnrollmentsByUser(userId);
        if (null != enrollmentDocuments && !enrollmentDocuments.isEmpty()) {
            return findCoursesById(enrollmentDocuments);
        }
        log.error("No available enrollments for userId " + userId);
        throw new NotFoundException("No available enrollments for userId " + userId);
    }

    private List<Enrollment> getEnrollmentsByUser(String userId) {
        return enrollmentRepository.findAllByUserId(userId,
                Sort.by(new Sort.Order(Sort.Direction.DESC, "isActive"),
                        new Sort.Order(Sort.Direction.ASC, "enrollmentStartDate")));
    }

    @SneakyThrows
    private ResponseDto findCoursesById(List<Enrollment> enrollments) {
        String[] courseIds = new String[enrollments.size()];
        int i = 0;
        for (Enrollment enrollment : enrollments)
            courseIds[i++] = enrollment.getCourseId();
        ResponseEntity<ResponseDto> courseResponse = courseServiceClient.findCoursesByIds(courseIds);
        if (courseResponse.getStatusCode() == HttpStatus.OK) {
            return courseResponse.getBody();
        }
        log.error("course service feign call failed, status code: {}",
                courseResponse.getStatusCode());
        throw new ServiceUnavailable("Feign call failed");
    }
}
