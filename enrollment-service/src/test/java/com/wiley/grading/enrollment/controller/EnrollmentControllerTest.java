package com.wiley.grading.enrollment.controller;

import com.wiley.grading.enrollment.dto.ResponseDto;
import com.wiley.grading.enrollment.exception.NotFoundException;
import com.wiley.grading.enrollment.service.EnrollmentService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class EnrollmentControllerTest {

    @Mock
    EnrollmentService enrollmentService;
    @InjectMocks
    private final EnrollmentController enrollmentController = new EnrollmentController(enrollmentService);
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(enrollmentController).build();

    }

    @Test
    public void contextLoads() {
        Assertions.assertThat(enrollmentController).isNotNull();
    }

    @Test
    public void findEnrollmentsByUserShouldReturnResponse() throws Exception {
        Mockito.when(enrollmentService.findEnrollmentsByUserId(Mockito.any(String.class)))
                .thenReturn(ResponseDto.builder().build());
        mvc.perform(get("/enrollment/find-enrollments-by-user/12345")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }

    @Test
    void findByAssignmentAndQuestionNotFundException() throws Exception {
        Mockito.when(enrollmentService.findEnrollmentsByUserId(Mockito.any(String.class)))
                .thenThrow(new NotFoundException("12345"));
        mvc.perform(get("/answer/assignment/12345/question/12345")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}