package com.wiley.grading.enrollment.impl;

import com.wiley.grading.enrollment.document.Enrollment;
import com.wiley.grading.enrollment.dto.ResponseDto;
import com.wiley.grading.enrollment.exception.NotFoundException;
import com.wiley.grading.enrollment.exception.ServiceUnavailable;
import com.wiley.grading.enrollment.feign.CourseServiceClient;
import com.wiley.grading.enrollment.repository.EnrollmentRepository;
import com.wiley.grading.enrollment.service.EnrollmentService;
import com.wiley.grading.enrollment.service.impl.EnrollmentServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EnrollmentServiceImplTest {
    private final String sampleString = "123456";
    @Mock
    private EnrollmentRepository enrollmentRepository;
    @Mock
    private CourseServiceClient courseServiceClient;
    @InjectMocks
    private final EnrollmentService enrollmentService = new EnrollmentServiceImpl(enrollmentRepository, courseServiceClient);

    @SneakyThrows
    @BeforeEach
    void setUp() {

    }

    @SneakyThrows
    @Test
    void findEnrollmentsByUser() {
        List<Enrollment> enrollments = new ArrayList<>();
        Enrollment enrollment = new Enrollment();
        enrollment.setCourseId(sampleString);
        enrollments.add(enrollment);
        Mockito.when(enrollmentRepository.findAllByUserId(sampleString,
                Sort.by(new Sort.Order(Sort.Direction.DESC, "active"),
                        new Sort.Order(Sort.Direction.ASC, "enrollmentStartDate"))))
                .thenReturn(enrollments);
        Mockito.when(courseServiceClient.findCoursesByIds(Mockito.any(String[].class)))
                .thenReturn(ResponseEntity.ok().body(ResponseDto.builder().build()));
        assertEquals(ResponseDto.builder().build(),
                enrollmentService.findEnrollmentsByUserId(sampleString));
    }

    @Test
    void findEnrollmentsByUserNotFoundException() {
        List<Enrollment> Enrollments = new ArrayList<>();
        Mockito.when(enrollmentRepository.findAllByUserId(sampleString,
                Sort.by(new Sort.Order(Sort.Direction.DESC, "active"),
                        new Sort.Order(Sort.Direction.ASC, "enrollmentStartDate"))))
                .thenReturn(Enrollments);
        Assertions.assertThrows(NotFoundException.class, () -> {
            enrollmentService.findEnrollmentsByUserId(sampleString);
        });
    }

    @SneakyThrows
    @Test
    void findEnrollmentsByUserServiceUnavailable() {
        List<Enrollment> Enrollments = new ArrayList<>();
        Enrollment Enrollment = new Enrollment();
        Enrollment.setCourseId(sampleString);
        Enrollments.add(Enrollment);
        Mockito.when(enrollmentRepository.findAllByUserId(sampleString,
                Sort.by(new Sort.Order(Sort.Direction.DESC, "active"),
                        new Sort.Order(Sort.Direction.ASC, "enrollmentStartDate"))))
                .thenReturn(Enrollments);
        Mockito.when(courseServiceClient.findCoursesByIds(Mockito.any(String[].class)))
                .thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(ResponseDto.builder().build()));
        Assertions.assertThrows(ServiceUnavailable.class, () -> {
            enrollmentService.findEnrollmentsByUserId(sampleString);
        });
    }

}