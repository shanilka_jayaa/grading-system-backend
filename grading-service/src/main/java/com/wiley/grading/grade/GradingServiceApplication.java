package com.wiley.grading.grade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradingServiceApplication.class, args);
	}

}
