package com.wiley.grading.grade.controller;

import com.wiley.grading.grade.dto.ResponseDto;
import com.wiley.grading.grade.service.GradingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("grade")
public class GradingController {
    private final GradingService gradingService;

    public GradingController(GradingService gradingService) {
        this.gradingService = gradingService;
    }

    @GetMapping("find-by-assignment-and-user/{assignment-id}/{user-id}")
    public ResponseEntity<ResponseDto> findByAssignmentAndUser(@PathVariable("assignment-id") String assignmentId,
                                                               @PathVariable("user-id") String userId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(gradingService.findByAssignmentAndUser(assignmentId, userId))
                        .build());
    }

}
