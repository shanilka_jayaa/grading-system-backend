package com.wiley.grading.grade.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "grade")
public class Grade {
    @Id
    private ObjectId gradeId;
    @Indexed
    private String assignmentId;
    @Indexed
    private String userId;
    private String questionIndex;
    private String grade;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;

    public String getGradeId() {
        return this.gradeId.toString();
    }
}
