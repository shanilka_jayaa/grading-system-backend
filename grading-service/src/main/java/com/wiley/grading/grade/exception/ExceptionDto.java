package com.wiley.grading.grade.exception;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@JsonDeserialize(builder = ExceptionDto.ExceptionDtoBuilder.class)
@Builder
@Value
public class ExceptionDto {
    private String message;

    @JsonPOJOBuilder(withPrefix = "")
    public static final class ExceptionDtoBuilder {

    }
}
