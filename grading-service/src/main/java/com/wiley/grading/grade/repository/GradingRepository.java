package com.wiley.grading.grade.repository;

import com.wiley.grading.grade.document.Grade;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface GradingRepository extends MongoRepository<Grade, String> {
    List<Grade> findAllByAssignmentIdAndUserId(String assignmentId, String userId, Sort questionNo);
}
