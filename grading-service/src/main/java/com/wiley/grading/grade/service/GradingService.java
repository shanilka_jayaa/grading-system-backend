package com.wiley.grading.grade.service;

import com.wiley.grading.grade.document.Grade;

import java.util.List;

public interface GradingService {
    List<Grade> findByAssignmentAndUser(String assignmentId, String userId);
}
