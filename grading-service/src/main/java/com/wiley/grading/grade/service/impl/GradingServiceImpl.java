package com.wiley.grading.grade.service.impl;

import com.wiley.grading.grade.document.Grade;
import com.wiley.grading.grade.exception.NotFoundException;
import com.wiley.grading.grade.repository.GradingRepository;
import com.wiley.grading.grade.service.GradingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class GradingServiceImpl implements GradingService {
    private final GradingRepository gradingRepository;

    public GradingServiceImpl(GradingRepository gradingRepository) {
        this.gradingRepository = gradingRepository;
    }

    @Override
    public List<Grade> findByAssignmentAndUser(String assignmentId, String userId) {
        List<Grade> gradeDocuments = gradingRepository.findAllByAssignmentIdAndUserId(assignmentId, userId, Sort.by(
                new Sort.Order(Sort.Direction.ASC, "questionIndex")));
        if (gradeDocuments != null && !gradeDocuments.isEmpty()){
            return gradeDocuments;
        }
        log.error("No grades found for user:" + userId + " assignment:" + assignmentId);
        throw new NotFoundException("No grades found for user:" + userId + " assignment:" + assignmentId);
    }
}
