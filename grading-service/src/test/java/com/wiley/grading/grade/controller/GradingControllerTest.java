package com.wiley.grading.grade.controller;

import com.wiley.grading.grade.document.Grade;
import com.wiley.grading.grade.exception.NotFoundException;
import com.wiley.grading.grade.service.GradingService;
import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class GradingControllerTest {

    private MockMvc mvc;

    @Mock
    private GradingService gradingService;

    @InjectMocks
    private final GradingController gradingController = new GradingController(gradingService);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(gradingController).build();
    }

    @Test
    public void contextLoads() {
        Assertions.assertThat(gradingController).isNotNull();
    }

    @Test
    void findByAssignmentAndUserShouldReturnResponse() throws Exception {
        List<Grade> gradeList = new ArrayList<>();
        Grade grade = new Grade();
        grade.setGradeId(ObjectId.get());
        gradeList.add(grade);
        Mockito.when(gradingService.findByAssignmentAndUser("12345", "12345"))
                .thenReturn(gradeList);
        mvc.perform(get("/grade/find-by-assignment-and-user/12345/12345")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }

    @Test
    void findByAssignmentAndUserNotFundException() throws Exception {
        Mockito.when(gradingService.findByAssignmentAndUser("12345", "12345"))
                .thenThrow(new NotFoundException("12345"));
        mvc.perform(get("/grade/find-by-assignment-and-user/12345/12345")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}