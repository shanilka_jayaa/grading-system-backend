package com.wiley.grading.grade.impl;

import com.wiley.grading.grade.document.Grade;
import com.wiley.grading.grade.exception.NotFoundException;
import com.wiley.grading.grade.repository.GradingRepository;
import com.wiley.grading.grade.service.GradingService;
import com.wiley.grading.grade.service.impl.GradingServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class GradingServiceImplTest {

    private final String sampleString = "123456";
    @Mock
    private GradingRepository gradingRepository;
    @InjectMocks
    private final GradingService gradingService = new GradingServiceImpl(gradingRepository);

    @BeforeEach
    void setUp() {
    }

    @Test
    void findByAssignmentAndUserValidResponse() {
        List<Grade> gradeList = new ArrayList<>();
        Grade grade = new Grade();
        grade.setGradeId(ObjectId.get());
        gradeList.add(grade);
        Mockito.when(gradingRepository.findAllByAssignmentIdAndUserId(sampleString, sampleString,
                Sort.by(new Sort.Order(Sort.Direction.ASC, "questionNo"))))
                .thenReturn(gradeList);
        assertEquals(gradeList,
                gradingService.findByAssignmentAndUser(sampleString, sampleString));
    }

    @Test
    void findByAssignmentAndUserNotFoundException() {
        List<Grade> gradeDocuments = new ArrayList<>();
        Mockito.when(gradingRepository.findAllByAssignmentIdAndUserId(sampleString, sampleString,
                Sort.by(new Sort.Order(Sort.Direction.ASC, "questionNo"))))
                .thenReturn(gradeDocuments);
        Assertions.assertThrows(NotFoundException.class, () -> {
            gradingService.findByAssignmentAndUser(sampleString, sampleString);
        });

    }
}