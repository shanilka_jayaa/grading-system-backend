package com.wiley.grading.qna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionAnswerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionAnswerServiceApplication.class, args);
	}

}
