package com.wiley.grading.qna.controller;

import com.wiley.grading.qna.dto.ResponseDto;
import com.wiley.grading.qna.service.QuestionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("question")
public class QuestionController {
    private final QuestionService questionService;


    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("assignment/{assignment-id}")
    public ResponseEntity<ResponseDto> findByAssignmentId(
            @PathVariable("assignment-id") String assignmentId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(questionService.findQuestionsByAssignmentId(assignmentId))
                        .build());

    }
}
