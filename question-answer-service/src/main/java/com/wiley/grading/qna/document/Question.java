package com.wiley.grading.qna.document;

import com.wiley.grading.qna.dto.AnswerDto;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Document(collection = "question")
public class Question {
    @Id
    private ObjectId questionId;
    @Indexed
    private String assignmentId;
    private String index;
    private String description;
    private String duration;
    private Boolean haveMultipleAnswers;
    private List<AnswerDto> answers;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;

    public String getQuestionId() {
        return this.questionId.toString();
    }
}
