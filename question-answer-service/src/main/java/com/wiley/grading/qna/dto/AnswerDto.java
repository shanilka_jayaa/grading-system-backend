package com.wiley.grading.qna.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerDto {
    String description;
    Boolean isCorrectAnswer;
    String answerIndex;
}
