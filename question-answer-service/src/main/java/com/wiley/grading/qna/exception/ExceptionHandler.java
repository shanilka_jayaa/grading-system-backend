package com.wiley.grading.qna.exception;

import com.wiley.grading.qna.dto.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ResponseDto> handleNotFoundException(
            Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ResponseDto
                        .builder()
                        .status("error")
                        .data(ExceptionDto.builder().message(ex.getMessage()).build())
                        .build());

    }
}
