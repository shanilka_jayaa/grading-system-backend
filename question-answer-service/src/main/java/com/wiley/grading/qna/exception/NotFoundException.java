package com.wiley.grading.qna.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No record found")
public class NotFoundException extends RuntimeException {

    public NotFoundException(String error) {
        super(error);
    }
}
