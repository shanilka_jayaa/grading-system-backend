package com.wiley.grading.qna.repository;

import com.wiley.grading.qna.document.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface QuestionRepository extends MongoRepository<Question, String> {
    Optional<List<Question>> findAllByAssignmentId(String assignmentId);
}
