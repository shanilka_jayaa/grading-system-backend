package com.wiley.grading.qna.service;

import com.wiley.grading.qna.document.Question;

import java.util.List;

public interface QuestionService {
    List<Question> findQuestionsByAssignmentId(String assignmentId);
}
