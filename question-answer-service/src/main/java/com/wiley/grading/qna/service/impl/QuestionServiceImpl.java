package com.wiley.grading.qna.service.impl;

import com.wiley.grading.qna.document.Question;
import com.wiley.grading.qna.exception.NotFoundException;
import com.wiley.grading.qna.repository.QuestionRepository;
import com.wiley.grading.qna.service.QuestionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository questionRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<Question> findQuestionsByAssignmentId(String assignmentId) {
        Optional<List<Question>> allByAssignmentId = questionRepository.findAllByAssignmentId(assignmentId);
        if (allByAssignmentId.isPresent()){
            return allByAssignmentId.get();
        }
        log.debug("Questions not found for assignmentId {}", assignmentId);
        throw new NotFoundException("Questions not found for assignmentId " + assignmentId);
    }
}
