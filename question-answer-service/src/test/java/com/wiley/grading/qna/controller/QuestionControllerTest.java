package com.wiley.grading.qna.controller;

import com.wiley.grading.qna.document.Question;
import com.wiley.grading.qna.service.QuestionService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class QuestionControllerTest {

    private MockMvc mvc;

    @Mock
    private QuestionService questionService;

    @InjectMocks
    private final QuestionController questionController = new QuestionController(questionService);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(questionController).build();
    }

    @Test
    void findByAssignmentIdShouldReturnResponse() throws Exception {
        List<Question> questions = new ArrayList<>();
        Question questionDocument = new Question();
        questionDocument.setQuestionId(ObjectId.get());
        questions.add(questionDocument);
        Mockito.when(questionService.findQuestionsByAssignmentId("test"))
                .thenReturn(questions);
        mvc.perform(get("/question/assignment/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }
}