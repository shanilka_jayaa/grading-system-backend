package com.wiley.grading.qna.impl;

import com.wiley.grading.qna.document.Question;
import com.wiley.grading.qna.repository.QuestionRepository;
import com.wiley.grading.qna.service.QuestionService;
import com.wiley.grading.qna.service.impl.QuestionServiceImpl;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class QuestionServiceImplTest {
    @Mock
    private QuestionRepository questionRepository;

    @InjectMocks
    private final QuestionService questionService = new QuestionServiceImpl(questionRepository);

    @Test
    void findQuestionsByAssignmentIdValidResponse() {
        List<Question> questions = new ArrayList<>();
        Question question = new Question();
        question.setQuestionId(ObjectId.get());
        questions.add(question);
        Optional<List<Question>> optional = Optional.of(questions);
        Mockito.when(questionRepository.findAllByAssignmentId("test"))
                .thenReturn(optional);
        assertEquals(questions,
                questionService.findQuestionsByAssignmentId("test"));
    }
}