package com.wiley.grading.student.ans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentAnswerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentAnswerServiceApplication.class, args);
	}

}
