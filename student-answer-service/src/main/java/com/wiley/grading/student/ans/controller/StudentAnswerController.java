package com.wiley.grading.student.ans.controller;


import com.wiley.grading.student.ans.dto.ResponseDto;
import com.wiley.grading.student.ans.service.StudentAnswerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("answer")
public class StudentAnswerController {
    private final StudentAnswerService studentAnswerService;

    public StudentAnswerController(StudentAnswerService studentAnswerService) {
        this.studentAnswerService = studentAnswerService;
    }

    @GetMapping("assignment/{assignment-id}/user/{user-id}")
    public ResponseEntity<ResponseDto> findByAssignmentAndUser(
            @PathVariable("assignment-id") String assignmentId,
            @PathVariable("user-id") String userId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(studentAnswerService.findByAssignmentAndUser(assignmentId, userId))
                        .build());
    }

}
