package com.wiley.grading.student.ans.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "studentAnswer")
public class StudentAnswer {
    @Id
    private ObjectId studentAnswerId;
    @Indexed
    private String assignmentId;
    @Indexed
    private String questionId;
    @Indexed
    private String userId;
    private String[] answerIndexes;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;

    public String getStudentAnswerId() {
        return studentAnswerId.toString();
    }
}
