package com.wiley.grading.student.ans.repository;

import com.wiley.grading.student.ans.document.StudentAnswer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface StudentAnswerRepository extends MongoRepository<StudentAnswer, String> {

    Optional<List<StudentAnswer>> findByAssignmentIdAndUserId(String assignmentId, String userId);
}
