package com.wiley.grading.student.ans.service;


import com.wiley.grading.student.ans.document.StudentAnswer;

import java.util.List;

public interface StudentAnswerService {
    List<StudentAnswer> findByAssignmentAndUser(String assignmentId, String userId);
}
