package com.wiley.grading.student.ans.service.impl;


import com.wiley.grading.student.ans.document.StudentAnswer;
import com.wiley.grading.student.ans.exception.NotFoundException;
import com.wiley.grading.student.ans.repository.StudentAnswerRepository;
import com.wiley.grading.student.ans.service.StudentAnswerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class StudentAnswerServiceImpl implements StudentAnswerService {
    private final StudentAnswerRepository studentAnswerRepository;

    public StudentAnswerServiceImpl(StudentAnswerRepository studentAnswerRepository) {
        this.studentAnswerRepository = studentAnswerRepository;
    }

    @Override
    public List<StudentAnswer> findByAssignmentAndUser(String assignmentId, String userId) {
        Optional<List<StudentAnswer>> answerDocument = studentAnswerRepository
                .findByAssignmentIdAndUserId(assignmentId, userId);
        if (answerDocument.isPresent()) {
            return answerDocument.get();
        }
        log.error("Student answers not found for user:" + userId + "and assignment:" + assignmentId);
        throw new NotFoundException("Student answers not found for user:" + userId + "and assignment:" + assignmentId);
    }
}
