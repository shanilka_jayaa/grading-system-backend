package com.wiley.grading.student.ans.controller;

import com.wiley.grading.student.ans.document.StudentAnswer;
import com.wiley.grading.student.ans.exception.NotFoundException;
import com.wiley.grading.student.ans.service.StudentAnswerService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class StudentAnswerControllerTest {
    private MockMvc mvc;

    @Mock
    private StudentAnswerService studentAnswerService;

    @InjectMocks
    private final StudentAnswerController studentAnswerController = new StudentAnswerController(studentAnswerService);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(studentAnswerController).build();
    }

    @Test
    void findByAssignmentAndQuestionAndUserShouldReturnResponse() throws Exception {
        List<StudentAnswer> studentAnswers = new ArrayList<>();
        StudentAnswer studentAnswer = new StudentAnswer();
        studentAnswer.setStudentAnswerId(ObjectId.get());
        studentAnswer.setAnswerIndexes(new String[]{"test"});
        studentAnswers.add(studentAnswer);
        Mockito.when(studentAnswerService.findByAssignmentAndUser(
                "test", "test"))
                .thenReturn(studentAnswers);
        mvc.perform(get("/answer/assignment/test/user/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }

    @Test
    void findByAssignmentAndQuestionAndUserNotFundException() throws Exception {
        Mockito.when(studentAnswerService.findByAssignmentAndUser(
                Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new NotFoundException("test"));
        mvc.perform(get("/answer/assignment/test/user/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}