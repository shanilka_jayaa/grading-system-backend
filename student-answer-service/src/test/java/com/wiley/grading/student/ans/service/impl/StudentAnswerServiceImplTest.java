package com.wiley.grading.student.ans.service.impl;

import com.wiley.grading.student.ans.document.StudentAnswer;
import com.wiley.grading.student.ans.exception.NotFoundException;
import com.wiley.grading.student.ans.repository.StudentAnswerRepository;
import com.wiley.grading.student.ans.service.StudentAnswerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class StudentAnswerServiceImplTest {

    @Mock
    private StudentAnswerRepository studentAnswerRepository;


    @InjectMocks
    private final StudentAnswerService studentAnswerService = new StudentAnswerServiceImpl(studentAnswerRepository);

    @Test
    void findByAssignmentAndQuestionAndUserValidResponse() {
        Optional<List<StudentAnswer>> answerList = Optional.of(new ArrayList<>());
        Mockito.when(studentAnswerRepository.findByAssignmentIdAndUserId(Mockito.anyString(),
                Mockito.anyString()))
                .thenReturn(answerList);
        assertEquals(answerList.get(),
                studentAnswerService.findByAssignmentAndUser(Mockito.anyString(),
                        Mockito.anyString()));
    }

    @Test
    void findByAssignmentAndQuestionAndUserNotFoundException() {
        Optional<List<StudentAnswer>> answers = Optional.empty();
        Mockito.when(studentAnswerRepository.findByAssignmentIdAndUserId(Mockito.anyString(),
                Mockito.anyString()))
                .thenReturn(answers);
        Assertions.assertThrows(NotFoundException.class, () -> {
            studentAnswerService.findByAssignmentAndUser(Mockito.anyString(),
                    Mockito.anyString());
        });
    }
}