package com.wiley.grading.user.controller;

import com.wiley.grading.user.dto.ResponseDto;
import com.wiley.grading.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("find-all")
    public ResponseEntity<ResponseDto> findAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.builder().status("success")
                        .data(userService.findAll())
                        .build());
    }
}
