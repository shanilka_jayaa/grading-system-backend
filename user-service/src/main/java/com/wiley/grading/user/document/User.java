package com.wiley.grading.user.document;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "user")
public class User {
    @Id
    private ObjectId userId;
    private String username;
    private String firstName;
    private String email;
    private String lastName;
    private String role;
    private String createdDate;
    private String modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private Boolean isActive;

    public String getUserId() {
        return this.userId.toString();
    }

}
