package com.wiley.grading.user.repository;

import com.wiley.grading.user.document.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
}
