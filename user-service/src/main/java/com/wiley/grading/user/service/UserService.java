package com.wiley.grading.user.service;

import com.wiley.grading.user.document.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    void saveUser(User user);
}
