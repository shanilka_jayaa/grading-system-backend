package com.wiley.grading.user.controller;

import com.wiley.grading.user.document.User;
import com.wiley.grading.user.service.UserService;
import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class UserControllerTest {
    private MockMvc mvc;
    @Mock
    private UserService userService;
    @InjectMocks
    private final UserController userController = new UserController(userService);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void contextLoads() {
        Assertions.assertThat(userController).isNotNull();
    }

    @Test
    void findAllUsersShouldReturnResponse() throws Exception {
        List<User> userList = new ArrayList<>();
        User user1 = new User();
        user1.setUserId(ObjectId.get());
        User user2 = new User();
        user2.setUserId(ObjectId.get());
        userList.add(user1);
        userList.add(user2);

        Mockito.when(userService.findAll()).thenReturn(userList);
        mvc.perform(get("/user/find-all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("success")));
    }
}