package com.wiley.grading.user.service.impl;

import com.wiley.grading.user.document.User;
import com.wiley.grading.user.repository.UserRepository;
import com.wiley.grading.user.service.UserService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private final UserService userService = new UserServiceImpl(userRepository);

    @BeforeEach
    void setUp() {

    }

    @Test
    void findAllUsersValidResponse() {
        List<User> userList = new ArrayList<>();
        User user1 = new User();
        user1.setUserId(ObjectId.get());
        User user2 = new User();
        user2.setUserId(ObjectId.get());
        userList.add(user1);
        userList.add(user2);

        Mockito.when(userRepository.findAll())
                .thenReturn(userList);
        assertEquals(userList,
                userService.findAll());
    }
}